using Gtk;
public class Sas : GLib.Object { 
    Button btn;
    public TextView input;
    public TextView output;
    public Switch swch;
    public Entry lang_to;
    public Entry lang_from;

    public void translate( Button source){
        string uri = "https://translate.yandex.net/api/v1.5/tr.json/translate";
        string API = "trnsl.1.1.xxxxxxxxxxxxx.xxxxx...";// u can get key here https://developer.tech.yandex.ru/
        string text = input.buffer.text; //print (@"$text\n");

        string lang = "";
        if (swch.active == false) lang =  @"$(lang_from.get_text())-$(lang_to.get_text())";
        else  lang =  @"$(lang_to.get_text())-$(lang_from.get_text())";

        string full_uri = @"$uri?key=$API&text=$(Soup.URI.encode (text, null))&lang=$lang";
        string translated_text = "";


        var message = new Soup.Message ("GET", full_uri);
        new Soup.Session ().send_message (message);
        var parser = new Json.Parser ();
        parser.load_from_data ((string) message.response_body.flatten ().data, -1);
        var root_object = parser.get_root ().get_object ();
        translated_text = root_object.get_array_member ("text")
                                     .get_string_element(0);
        output.buffer.text = translated_text;
    }

    public Sas() {
        var builder = new Builder();
        builder.add_from_file("sas.ui");

        var window = builder.get_object("window")   as Window;
        btn = builder.get_object("btn")             as Button;
        input = builder.get_object("input")         as TextView;
        output = builder.get_object("output")       as TextView;
        swch = builder.get_object("switch")         as Switch;
        lang_from = builder.get_object("lang_from") as Entry;
        lang_to = builder.get_object("lang_to")     as Entry;

        btn.clicked.connect(translate);
        window.destroy.connect (Gtk.main_quit);
        window.show_all();
    }
}

public static int main(string[] args) {
    Gtk.init (ref args);
    var app = new Sas();
    Gtk.main();
    return 0;
}